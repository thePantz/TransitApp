package com.example.beast2.transitapp;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;


public class MainActivity extends ActionBarActivity implements KeyResponse, DatePickerDialog.DatePickerDialogListener{

    private String origin, destination, originKey, destinationKey;
    private TextView dateText;

    private LinearLayout destinationEditLayout, destinationDisplayLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        destinationEditLayout      = (LinearLayout)findViewById(R.id.layout_edit_destination);
        destinationDisplayLayout   = (LinearLayout)findViewById(R.id.layout_display_destination);
        dateText = (TextView)findViewById(R.id.text_date);

        Spinner spinnerTimeOptions = (Spinner)findViewById(R.id.list_time);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.time_array, android.R.layout.simple_spinner_dropdown_item);
        spinnerTimeOptions.setAdapter(adapter);

    }


    public void submitOrigin(View v)
    {
        EditText originEdit = (EditText)findViewById(R.id.edit_origin);
        origin = originEdit.getText().toString();

        if("".equals(origin))//eliminates the need to check for null
        {
            Toast.makeText(MainActivity.this, "Please enter an origin", Toast.LENGTH_LONG).show();
        }
        else
        {
            AddressKeyFinder originFinder = new AddressKeyFinder(origin, true, this);
            originFinder.execute();

        }

    }


    public void submitDestination(View v)
    {
        EditText destinationEdit = (EditText)findViewById(R.id.edit_destination);
        destination = destinationEdit.getText().toString();

        if("".equals(destination))
        {
            Toast.makeText(MainActivity.this, "Please enter a destination", Toast.LENGTH_LONG).show();
        }
        else
        {
            AddressKeyFinder destinationFinder = new AddressKeyFinder(destination, false, this);
            destinationFinder.execute();
        }

    }

    //used by KeyResponse interface to return key from AddressKeyFinder
    public void getKey(String key, String name, boolean orgDest)
    {
        if(key.equals("NO RESULTS FOUND"))
        {
            Toast.makeText(MainActivity.this, "No results found, please try again", Toast.LENGTH_LONG).show();
        }
        else if(key.equals("MULTIPLE RESULTS FOUND"))
        {
            //do nothing
        }
        else if(orgDest)
        {
            originKey = key;
            setOriginLayout(true, name);
        }
        else
        {
            destinationKey = key;
            setDestinationLayout(true, name);
        }
    }

    //opens dialog when multiple results are found
    public void resolveSearch(ArrayList<String> items, final boolean orgDest)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.dialog_list, null);
        builder.setView(convertView);

        //set dialog title
        builder.setTitle("Did you mean:");

        //if back button is pressed, dialog is not canceled
        builder.setCancelable(false);

        //convert array list to string array
        String[] itemArray = new String[items.size()];
        final String[] ITEMS = items.toArray(itemArray);

        //populate list view
        ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, ITEMS );

        ListView list = (ListView)convertView.findViewById(R.id.dialog_listview);
        list.setAdapter(adapter);

        final AlertDialog alert = builder.create();
        Button cancelButton = (Button)convertView.findViewById(R.id.cancel_button);

        //if cancel button is clicked, close dialog
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


        //if list item is clicked, search for item
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = ITEMS[position];

                if(orgDest)
                {
                    origin = selectedItem;
                }
                else
                {
                    destination = selectedItem;
                }

                AddressKeyFinder finder = new AddressKeyFinder(selectedItem,orgDest, MainActivity.this);
                finder.execute();
                alert.dismiss();

            }
        });

        //show dialog
        alert.show();


    }

    private void setOriginLayout(boolean showDisplayLayout, String name)
    {
        origin = name;

        setOriginLayout(showDisplayLayout);
    }
    private void setOriginLayout(boolean showDisplayLayout)
    {
        EditText originEditText                 = (EditText)findViewById(R.id.edit_origin);
        TextView originDisplayText              = (TextView)findViewById(R.id.text_origin_display);
        EditText destinationEditText            = (EditText)findViewById(R.id.edit_destination);
        LinearLayout originEditLayout           = (LinearLayout)findViewById(R.id.layout_edit_origin);
        LinearLayout originDisplayLayout        = (LinearLayout)findViewById(R.id.layout_display_origin);
        LinearLayout destinationEditLayout      = (LinearLayout)findViewById(R.id.layout_edit_destination);
        LinearLayout destinationDisplayLayout   = (LinearLayout)findViewById(R.id.layout_display_destination);

        if(showDisplayLayout)
        {
            originDisplayText.setText(origin);
            originEditLayout.setVisibility(View.GONE);
            originDisplayLayout.setVisibility(View.VISIBLE);

            //if destination edit layout has not been displayed yet, display it
            if(destinationEditLayout.getVisibility() != View.VISIBLE && destinationDisplayLayout.getVisibility() != View.VISIBLE)
            {
                destinationEditLayout.setVisibility(View.VISIBLE);
                destinationEditText.requestFocus();
            }

        }
        else //show edit layout
        {
            originEditText.setText(originDisplayText.getText().toString());
            originEditLayout.setVisibility(View.VISIBLE);
            originDisplayLayout.setVisibility(View.GONE);
        }


    }


    private void setDestinationLayout(boolean showDisplayLayout, String name)
    {
        destination = name;

        setDestinationLayout(showDisplayLayout);
    }

    private void setDestinationLayout(boolean showDisplayLayout)
    {
        EditText destinationEditText = (EditText)findViewById(R.id.edit_destination);
        TextView destinationDisplayText = (TextView)findViewById(R.id.text_destination_display);
        LinearLayout timeLayout = (LinearLayout)findViewById(R.id.layout_time);

        if(showDisplayLayout)
        {
            destinationDisplayText.setText(destination);
            destinationEditLayout.setVisibility(View.GONE);
            destinationDisplayLayout.setVisibility(View.VISIBLE);
            timeLayout.setVisibility(View.VISIBLE);
            setCurrentTime();
        }
        else
        {
            destinationEditText.setText(destinationDisplayText.getText().toString());
            destinationEditLayout.setVisibility(View.VISIBLE);
            destinationDisplayLayout.setVisibility(View.GONE);
        }

    }

    public void changeOrigin(View v)
    {
        setOriginLayout(false);
    }

    public void changeDestination(View v){ setDestinationLayout(false);}


    public void setCurrentTime()
    {
        String day,year;
        TextView dateText = (TextView)findViewById(R.id.text_date);
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.time_period);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat twelveHrFormat = new SimpleDateFormat("hh:mm");
        String twelveHrTime = twelveHrFormat.format(c.getTime());

        EditText time = (EditText)findViewById(R.id.time);
        time.setText(twelveHrTime);

        day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        year = String.valueOf(c.get(Calendar.YEAR));

        dateText.setText(String.valueOf(c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.CANADA)) + " " + day + ", " + year);


        //set period
        if(c.get(Calendar.HOUR_OF_DAY) >= 12)
        {
            radioGroup.check(R.id.pm);
        }
        else
        {
            radioGroup.check(R.id.am);
        }

    }


    public void showDatePickerDialog(View v)
    {
        FragmentManager fm = getFragmentManager();
        DatePickerDialog datePickerDialog = new DatePickerDialog();
        datePickerDialog.show(fm, "fragment_datepicker");
    }

    @Override
    public void onFinishDateDialog(Calendar c) {
        //format date
        String day, year;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        day = String.valueOf(c.get(Calendar.DAY_OF_MONTH));
        year = String.valueOf(c.get(Calendar.YEAR));

        dateText.setText(String.valueOf(c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.CANADA)) + " " + day + ", " + year);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
