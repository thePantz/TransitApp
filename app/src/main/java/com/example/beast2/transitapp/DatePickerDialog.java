package com.example.beast2.transitapp;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Created by Beast2 on 12/31/2014.
 */
public class DatePickerDialog extends DialogFragment {
    public interface DatePickerDialogListener{
        void onFinishDateDialog(Calendar c);
    }

    private DatePicker datePicker;

    public DatePickerDialog()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_datepicker, container);
        datePicker = (DatePicker) view.findViewById(R.id.date_picker);
        getDialog().setTitle("Change Date");

        return view;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        DatePickerDialogListener activity = (DatePickerDialogListener) getActivity();

        Calendar c = Calendar.getInstance();
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        c.set(year, month, day);

        activity.onFinishDateDialog(c);

    }
}
