package com.example.beast2.transitapp;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.helpers.DefaultHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by Beast2 on 12/26/2014.
 */
public class JSONHandler extends DefaultHandler{

    static InputStream inputStream = null;
    static JSONObject jObj = null;
    static String inString = "";

    public JSONObject getJSONFromURL(String url){

        //make HTTP request using passed url
        try{
            //create http client object
            DefaultHttpClient httpClient = new DefaultHttpClient();

            //create httpPost object
            HttpGet httpGet = new HttpGet(url);

            //execute get on client.
            //returns a response
            HttpResponse httpResponse = httpClient.execute(httpGet);

            //get content from response
            HttpEntity httpEntity = httpResponse.getEntity();
            inputStream = httpEntity.getContent();
        } catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (ClientProtocolException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

            StringBuilder sb = new StringBuilder();
            String line = null;

            //read every line in reader, append to string builder
            while((line = reader.readLine()) != null){
                sb.append(line);
            }

            inputStream.close();
            inString = sb.toString();
        }catch (Exception e){
            Log.e("Buffer Error", "Error converting result " + e.toString());
            e.printStackTrace();
        }

        //try to parse string to JSONObject
        try{
            jObj = new JSONObject(inString);
        }catch(JSONException e){
            Log.e("JSON Parse Error", "Error Parsing String to JSON ");
            e.printStackTrace();
        }

        return jObj;

    }

    public JSONObject getParsedJSON() {return jObj;}
}
