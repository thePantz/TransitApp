package com.example.beast2.transitapp;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Beast2 on 12/27/2014.
 */


public class AddressKeyFinder extends AsyncTask<Void,Void,Void>  {
    private JSONHandler handler;
    private String address;
    private boolean orgDest;
    private KeyResponse delegate;

    AddressKeyFinder(String address, boolean orgDest, KeyResponse delegate)
    {
        this.address = address;
        this.orgDest = orgDest;
        this.delegate = delegate;
    }

    @Override
    protected Void doInBackground(Void... params) {
        handler = new JSONHandler();
        final String API_KEY = "M1PmHsWTXQfKXEXNPMG";

        String formattedAddress = formatAddress(address);

        handler.getJSONFromURL("http://api.winnipegtransit.com/v2/locations:"+formattedAddress+".json?api-key="+API_KEY);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        String key;
        String name = "";
        try
        {
            //get key
            JSONObject json = handler.getParsedJSON();
            JSONArray locations = json.optJSONArray("locations");


            if(locations == null) //if no locations are found
            {
                key = "NO RESULTS FOUND";
            }
            else if(locations.length() > 1) //if more than one location is returned
            {
                key = "MULTIPLE RESULTS FOUND";
                ArrayList<String> locationList = generateLocationList(locations);

                delegate.resolveSearch(locationList, orgDest);
            }
            else //if a single location is found
            {
                JSONObject location = locations.getJSONObject(0);

                name = getName(location);
                key = location.getString("key");
            }

            delegate.getKey(key, name, orgDest);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public String getName(JSONObject location) {

        String name = "";
        try{
            String type = location.getString("type");

            if(type.equals("address"))
            {
                String streetNumber = location.getString("street-number");
                JSONObject street = location.getJSONObject("street");

                name = streetNumber + " " + street.getString("name");

            }
            else if(type.equals("intersection"))
            {
                //get street name
                JSONObject street = location.getJSONObject("street");
                String streetName = street.getString("name");

                //get cross street name
                JSONObject crossStreet = location.getJSONObject("cross-street");
                String crossName = crossStreet.getString("name");

                name = streetName+"@"+crossName;
            }
            else if(type.equals("monument"))
            {
                name = location.getString("name");
            }
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return name;

    }


    private ArrayList<String> generateLocationList(JSONArray locations)
    {

        ArrayList<String> locationList = new ArrayList<String>();
        for(int i = 0; i< locations.length(); i++)
        {
            try
            {
                JSONObject location = locations.getJSONObject(i);
                locationList.add(getName(location));
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }

        return locationList;
    }

    //formats the address to be used in an api request
    private String formatAddress(String addr)
    {
        String formattedAddress;

        String[] splits = addr.split(" ");

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < splits.length; i++)
        {
            //if on the last search term, do not append a '+'
            if(i == splits.length-1)
            {
                sb.append(splits[i]);
            }
            else
            {
                sb.append(splits[i]+"+");
            }


        }

        formattedAddress = sb.toString();

        return formattedAddress;
    }
}
