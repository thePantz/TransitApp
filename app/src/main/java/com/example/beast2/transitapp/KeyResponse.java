package com.example.beast2.transitapp;

import java.util.ArrayList;

/**
 * Created by Beast2 on 12/27/2014.
 */
public interface KeyResponse{
    void getKey(String key, String name, boolean orgDest);
    void resolveSearch(ArrayList<String> items, boolean orgDest);
}

